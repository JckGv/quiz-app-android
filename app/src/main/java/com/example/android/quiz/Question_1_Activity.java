package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

public class Question_1_Activity extends AppCompatActivity {
    int score = 0;

    RadioButton buttonAnswerA, buttonAnswerB, buttonAnswerC, buttonAnswerD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_1);

        buttonAnswerA = (RadioButton) findViewById(R.id.answer1a);
        buttonAnswerB = (RadioButton) findViewById(R.id.answer1b);
        buttonAnswerC = (RadioButton) findViewById(R.id.answer1c);
        buttonAnswerD = (RadioButton) findViewById(R.id.answer1d);
    }

    /**
     * These methods are used to create a RadioGroup. It is specified here because it gives me freedom to
     * organize the button in the view in any way.
     */
    public void buttonAnswerA(View view) {
        buttonAnswerA.setChecked(true);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void buttonAnswerB(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(true);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void buttonAnswerC(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(true);
        buttonAnswerD.setChecked(false);
    }

    public void buttonAnswerD(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(true);
    }

    /**
     * This method is called when the button is pressed and starts a new activity.
     * The intent includes the putExtra to send updated score to new activity.
     */
    public void nextButton(View view) {
        RadioButton answerA = (RadioButton) findViewById(R.id.answer1a);
        boolean checkedAnswerA = answerA.isChecked();

        RadioButton answerB = (RadioButton) findViewById(R.id.answer1b);
        boolean checkedAnswerB = answerB.isChecked();

        RadioButton answerC = (RadioButton) findViewById(R.id.answer1c);
        boolean checkedAnswerC = answerC.isChecked();

        RadioButton answerD = (RadioButton) findViewById(R.id.answer1d);
        boolean checkedAnswerD = answerD.isChecked();


        Intent nextButton = new Intent(this, Question_2_Activity.class);
        int scoreValue = calculateScore(checkedAnswerA, checkedAnswerB, checkedAnswerC, checkedAnswerD);
        nextButton.putExtra("score", scoreValue);
        startActivity(nextButton);
        overridePendingTransition(0, 0);
    }

    /**
     * This method calculates the score based on the chosen answer.
     *
     * @param checkedA is the correct answer
     */
    public int calculateScore(boolean checkedA, boolean checkedB, boolean checkedC, boolean checkedD) {
        int scoreValue = score;
        if (checkedA) {
            scoreValue += 10;
        }
        if (checkedB) {
            scoreValue += 0;
        }
        if (checkedC) {
            scoreValue += 0;
        }
        if (checkedD) {
            scoreValue += 0;
        }
        return scoreValue;

    }


}
