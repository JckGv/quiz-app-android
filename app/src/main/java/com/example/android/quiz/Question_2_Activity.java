package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;


public class Question_2_Activity extends AppCompatActivity {
    int score = 0;


    RadioButton buttonAnswerA, buttonAnswerB, buttonAnswerC, buttonAnswerD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_2);

        buttonAnswerA = (RadioButton) findViewById(R.id.answer2a);
        buttonAnswerB = (RadioButton) findViewById(R.id.answer2b);
        buttonAnswerC = (RadioButton) findViewById(R.id.answer2c);
        buttonAnswerD = (RadioButton) findViewById(R.id.answer2d);


        Intent previousIntent = getIntent();
        Bundle extras = previousIntent.getExtras();
        score = extras.getInt("score", 0);


    }

    /**
     * These methods are used to create a RadioGroup. It is specified here because it gives me freedom to
     * organize the button in the view in any way.
     */
    public void setButtonAnswerA(View view) {
        buttonAnswerA.setChecked(true);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerB(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(true);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerC(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(true);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerD(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(true);
    }

    /**
     * This method is called when the button is pressed and starts a new activity and takes the scoreValue with.
     */

    public void nextButton(View view) {
        RadioButton answerA = (RadioButton) findViewById(R.id.answer2a);
        boolean answerAChecked = answerA.isChecked();

        RadioButton answerB = (RadioButton) findViewById(R.id.answer2b);
        boolean answerBChecked = answerB.isChecked();

        RadioButton answerC = (RadioButton) findViewById(R.id.answer2c);
        boolean answerCChecked = answerC.isChecked();

        RadioButton answerD = (RadioButton) findViewById(R.id.answer2d);
        boolean answerDChecked = answerD.isChecked();

        int scoreValue = calculateScore(answerAChecked, answerBChecked, answerCChecked, answerDChecked);

        Intent changeNextActivity = new Intent(this, Question_3_Activity.class);
        changeNextActivity.putExtra("score", scoreValue);
        startActivity(changeNextActivity);
        overridePendingTransition(0, 0);


    }

    /**
     * This method is used to calculate the score based on the chosen answer.
     *
     * @param checkedC is the correct answer.
     */

    public int calculateScore(boolean checkedA, boolean checkedB, boolean checkedC, boolean checkedD) {
        int scoreValue = score;
        if (checkedA) {
            scoreValue += 0;
        }
        if (checkedB) {
            scoreValue += 0;
        }
        if (checkedC) {
            scoreValue += 10;
        }
        if (checkedD) {
            scoreValue += 0;
        }
        return scoreValue;

    }


}
