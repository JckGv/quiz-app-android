package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

public class Question_3_Activity extends AppCompatActivity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_3);

        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);
    }

    /**
     * This method is called when the next button is pressed and starts a new activity sending the scoreValue along with it.
     */
    public void nextButton(View view) {
        RadioButton answerA = (RadioButton) findViewById(R.id.answer3a);
        boolean checkedAnswerA = answerA.isChecked();

        RadioButton answerB = (RadioButton) findViewById(R.id.answer3b);
        boolean checkedAnswerB = answerB.isChecked();

        RadioButton answerC = (RadioButton) findViewById(R.id.answer3c);
        boolean checkedAnswerC = answerC.isChecked();

        RadioButton answerD = (RadioButton) findViewById(R.id.answer3d);
        boolean checkedAnswerD = answerD.isChecked();

        Intent nextButton = new Intent(this, Question_4_Activity.class);
        int scoreValue = calculateScore(checkedAnswerA, checkedAnswerB, checkedAnswerC, checkedAnswerD);
        nextButton.putExtra("score", scoreValue);
        startActivity(nextButton);
        overridePendingTransition(0, 0);

    }

    /**
     * This method calculates the score.
     *
     * @param checkedB is the correct answer.
     */
    public int calculateScore(boolean checkedA, boolean checkedB, boolean checkedC, boolean checkedD) {
        int scoreValue = score;
        if (checkedA) {
            scoreValue += 0;
        }
        if (checkedB) {
            scoreValue += 10;
        }
        if (checkedC) {
            scoreValue += 0;
        }
        if (checkedD) {
            scoreValue += 0;
        }
        return scoreValue;
    }


}
