package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;

public class Question_4_Activity extends AppCompatActivity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_4);

        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);

    }

    /**
     * This method is called when the next button is pressed and starts a new activity sending the scoreValue along with it.
     */
    public void nextButton(View view) {
        CheckBox answerA = (CheckBox) findViewById(R.id.answer4a);
        boolean checkedAnswerA = answerA.isChecked();

        CheckBox answerB = (CheckBox) findViewById(R.id.answer4b);
        boolean checkedAnswerB = answerB.isChecked();

        CheckBox answerC = (CheckBox) findViewById(R.id.answer4c);
        boolean checkedAnswerC = answerC.isChecked();

        CheckBox answerD = (CheckBox) findViewById(R.id.answer4d);
        boolean checkedAnswerD = answerD.isChecked();

        Intent changeNextActivity = new Intent(this, Question_5_Activity.class);
        int scoreValue = calculateScore(checkedAnswerA, checkedAnswerB, checkedAnswerC, checkedAnswerD);
        changeNextActivity.putExtra("score", scoreValue);
        startActivity(changeNextActivity);
        overridePendingTransition(0, 0);
    }

    /**
     * This method calculates the score. In this case there are 2 correct answers.
     *
     * @param checkedA is correct.
     * @param checkedB is incorrect, if chosen will deduct points.
     * @param checkedC is correct.
     * @param checkedD is incorrect, if chosen will deduct points.
     */
    public int calculateScore(boolean checkedA, boolean checkedB, boolean checkedC, boolean checkedD) {
        int scoreValue = score;
        if (checkedA) {
            scoreValue += 8;
        }
        if (checkedB) {
            scoreValue -= 4;
        }
        if (checkedC) {
            scoreValue += 8;
        }
        if (checkedD) {
            scoreValue -= 4;
        }
        return scoreValue;

    }
}
