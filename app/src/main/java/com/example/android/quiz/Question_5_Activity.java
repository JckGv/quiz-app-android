package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

public class Question_5_Activity extends AppCompatActivity {
    int score = 0;

    RadioButton buttonAnswerA, buttonAnswerB, buttonAnswerC, buttonAnswerD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_5);

        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);

        buttonAnswerA = (RadioButton) findViewById(R.id.answer5a);
        buttonAnswerB = (RadioButton) findViewById(R.id.answer5b);
        buttonAnswerC = (RadioButton) findViewById(R.id.answer5c);
        buttonAnswerD = (RadioButton) findViewById(R.id.answer5d);

    }

    /**
     * These 4 methods are used to create a RadioGroup.
     * Defined here so the buttons can be organized however I want.
     */
    public void setButtonAnswerA(View view) {
        buttonAnswerA.setChecked(true);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerB(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(true);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerC(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(true);
        buttonAnswerD.setChecked(false);
    }

    public void setButtonAnswerD(View view) {
        buttonAnswerA.setChecked(false);
        buttonAnswerB.setChecked(false);
        buttonAnswerC.setChecked(false);
        buttonAnswerD.setChecked(true);
    }

    /**
     * This method is called when the next button is pressed to start a new activity and sends the scoreValue along with it.
     */
    public void nextButton(View view) {
        RadioButton answerA = (RadioButton) findViewById(R.id.answer5a);
        boolean checkedAnswerA = answerA.isChecked();

        RadioButton answerB = (RadioButton) findViewById(R.id.answer5b);
        boolean checkedAnswerB = answerB.isChecked();

        RadioButton answerC = (RadioButton) findViewById(R.id.answer5c);
        boolean checkedAnswerC = answerC.isChecked();

        RadioButton answerD = (RadioButton) findViewById(R.id.answer5d);
        boolean checkedAnswerD = answerD.isChecked();

        Intent changeNextActivity = new Intent(this, Question_6_Activity.class);

        int scoreValue = calculateScore(checkedAnswerA, checkedAnswerB, checkedAnswerC, checkedAnswerD);

        changeNextActivity.putExtra("score", scoreValue);
        startActivity(changeNextActivity);
        overridePendingTransition(0, 0);

    }

    /**
     * This method calculates the score.
     *
     * @param checkedC is the correct answer.
     */
    public int calculateScore(boolean checkedA, boolean checkedB, boolean checkedC, boolean checkedD) {
        int scoreValue = score;
        if (checkedA) {
            scoreValue += 0;
        }
        if (checkedB) {
            scoreValue += 0;
        }
        if (checkedC) {
            scoreValue += 10;
        }
        if (checkedD) {
            scoreValue += 0;
        }
        return scoreValue;

    }

}
