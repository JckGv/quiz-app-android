package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;

public class Question_6_Activity extends AppCompatActivity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_6);

        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);
    }

    /**
     * This method is called when the next button is pressed to create a new activity and sends the scoreValue along with it.
     */
    public void nextButton(View view) {
        CheckBox answerA = (CheckBox) findViewById(R.id.answer6a);
        boolean checkedAnswerA = answerA.isChecked();

        CheckBox answerB = (CheckBox) findViewById(R.id.answer6b);
        boolean checkedAnswerB = answerB.isChecked();

        CheckBox answerC = (CheckBox) findViewById(R.id.answer6c);
        boolean checkedAnswerC = answerC.isChecked();

        CheckBox answerD = (CheckBox) findViewById(R.id.answer6d);
        boolean checkedAnswerD = answerD.isChecked();

        Intent changeNextActivity = new Intent(this, Question_7_Activity.class);

        int scoreValue = calculateScore(checkedAnswerA, checkedAnswerB, checkedAnswerC, checkedAnswerD);

        changeNextActivity.putExtra("score", scoreValue);
        startActivity(changeNextActivity);
        overridePendingTransition(0, 0);

    }

    /**
     * This method calculates the score. It was multiple answers
     *
     * @param checkA is correct.
     * @param checkB is correct.
     * @param checkC is correct.
     * @param checkD is incorrect, points will be deducted if chosen.
     */
    public int calculateScore(boolean checkA, boolean checkB, boolean checkC, boolean checkD) {
        int scoreValue = score;
        if (checkA) {
            scoreValue += 8;
        }
        if (checkB) {
            scoreValue += 8;
        }
        if (checkC) {
            scoreValue += 8;
        }
        if (checkD) {
            scoreValue -= 4;
        }
        return scoreValue;
    }


}
