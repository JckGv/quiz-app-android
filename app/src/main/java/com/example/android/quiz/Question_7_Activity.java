package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

public class Question_7_Activity extends AppCompatActivity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_7);
        //This will allow the layout to resize when the inputMethod is pressed.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);

    }

    /**
     * This method is called when the Next button is pressed to start a new activity.
     * The intent sends the score to the new activity.
     */
    public void nextButton(View view) {
        EditText answer = (EditText) findViewById(R.id.textAnswer);
        String answerText = answer.getText().toString();

        Intent changeNextActivity = new Intent(this, Question_8_Activity.class);
        int scoreValue = calculateScore(answerText);
        changeNextActivity.putExtra("score", scoreValue);
        startActivity(changeNextActivity);
        overridePendingTransition(0, 0);

    }

    /**
     * This method calculates the score based on the answer.
     *
     * @param answerCorrect turns what is written by the user and puts it to lower case comparing it to the contains.
     */
    public int calculateScore(String answerCorrect) {
        int scoreValue = score;
        if (answerCorrect.toLowerCase().contains("low five ghost")) {
            scoreValue += 10;
        }
        return scoreValue;
    }


}
