package com.example.android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

public class Question_8_Activity extends AppCompatActivity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_8);
        //This will resize the layout when the keyboard appears.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);
    }

    /**
     * This method is called when the next button is pressed and starts a new activity and takes the score along with it.
     */
    public void nextButton(View view) {
        EditText correctAnswer = (EditText) findViewById(R.id.answerText);
        String textAnswer = correctAnswer.getText().toString();

        int scoreValue = calculateScore(textAnswer);

        Intent changeActivityNext = new Intent(this, ScoreActivity.class);
        changeActivityNext.putExtra("score", scoreValue);
        startActivity(changeActivityNext);
    }

    /**
     * This method calculates the score based on what the user wrote.
     *
     * @param answer turns the users input to lower case and compares it to see if it contains the same string.
     */

    public int calculateScore(String answer) {
        int scoreValue = score;
        if (answer.toLowerCase().contains("oh no, bro!")) {
            scoreValue += 10;
        }
        if (answer.toLowerCase().contains("oh no bro")) {
            scoreValue += 10;
        }

        return scoreValue;
    }


}
