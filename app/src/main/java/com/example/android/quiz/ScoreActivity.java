package com.example.android.quiz;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreActivity extends Question_8_Activity {
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);


        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score", 0);
        String messageDisplay = resultMessage();
        setDisplayScore(messageDisplay);

    }

    public String resultMessage() {
        String messageDisplay;
        if (score < 30) {
            messageDisplay = getString(R.string.scoreResult, score) + "\n" + getString(R.string.lowScoreMessage);
            ImageView lowScoreView = (ImageView) findViewById(R.id.imageScore);
            int width = 700;
            int height = 600;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            lowScoreView.setLayoutParams(params);
            lowScoreView.setBackgroundResource(R.drawable.first_score_image);
        } else if (score >= 30 && score < 50) {
            messageDisplay = getString(R.string.scoreResult, score) + "\n" + getString(R.string.secondLowScoreMessage);
            ImageView lowMediumScore = (ImageView) findViewById(R.id.imageScore);
            int width = 700;
            int height = 600;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            lowMediumScore.setLayoutParams(params);
            lowMediumScore.setBackgroundResource(R.drawable.second_score_image);

        } else if (score >= 50 && score < 70) {
            messageDisplay = getString(R.string.scoreResult, score) + "\n" + getString(R.string.mediumScoreMessage);
            ImageView mediumScore = (ImageView) findViewById(R.id.imageScore);
            int width = 700;
            int height = 600;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            mediumScore.setLayoutParams(params);
            mediumScore.setBackgroundResource(R.drawable.third_score_image);
        } else if (score >= 70 && score <= 99) {
            messageDisplay = getString(R.string.scoreResult, score) + "\n" + getString(R.string.mediumHighcoreMessage);
            ImageView mediumHighScore = (ImageView) findViewById(R.id.imageScore);
            int width = 700;
            int height = 600;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            mediumHighScore.setLayoutParams(params);
            mediumHighScore.setBackgroundResource(R.drawable.forth_score_image);
        } else {
            messageDisplay = getString(R.string.scoreResult, score) + "\n" + getString(R.string.highScoreMessage);
            ImageView highScore = (ImageView) findViewById(R.id.imageScore);
            int width = 500;
            int height = 700;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            highScore.setLayoutParams(params);
            highScore.setBackgroundResource(R.drawable.fifth_score_image);
        }
        return messageDisplay;
    }

    private void setDisplayScore(String score) {
        TextView displayScore = (TextView) findViewById(R.id.scoreMessage);
        displayScore.setText(String.valueOf(score));
    }


}
